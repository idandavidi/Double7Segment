/*********************************************************************
Read a string from the serial monitor window and display it on 
the double 7segment modules.

Written by: Idan Davidi
Date:       19.05.2018
*********************************************************************/

#define PIN_LATCH 8
#define PIN_CLOCK 10
#define PIN_DAI 11

#include "Double7Segment.h"

Double7Segment counter(PIN_LATCH, PIN_CLOCK, PIN_DAI);

void setup() {
  Serial.begin(9600);
  pinMode(PIN_LATCH, OUTPUT);
  pinMode(PIN_CLOCK, OUTPUT);
  pinMode(PIN_DAI, OUTPUT);
}

void loop() {
  while (Serial.available()) {
    String a = Serial.readString();
    a = a.substring(0, a.length()-1);
    Serial.println(a);
    counter.display(a);
  }
}
