/*********************************************************************
Double Seven Segment Controller, lead by DM13A constant current driver.

Written by: Idan Davidi
Date:       19.05.2018

The connection to the seven segment modules should be as follow:

    __9___             __1___      
   |      |           |      |     
10 |      | 8       2 |      | 0   
   |__11__|           |__3___|     
   |      |           |      |     
12 |      |  14     4 |      |  6  
   |______|  . 15     |______|  . 7
      13                 5         

The numbers are the OUT pins of the DM13A driver.
*********************************************************************/
#ifndef D7SEG_H
#define D7SEG_H

#include "Arduino.h"
#define DIGIT_OFF 0B00000000
#define DOT_SEGMENT_MASK 0B10000000
#define HYPHEN 0B000001000

class Double7Segment {
  // Each cell in the numbers array is the way the index is represented in the 7 segment module.
  // e.g, numbers[5] is the 7segment representation of 5.
  const byte numbers[10] = { 0B01110111, 0B01000001, 0B00111011, 0B01101011, 0B01001101, 
                             0B01101110, 0B01111110, 0B01000011, 0B01111111, 0B01101111 };
  String convertToFormattedNumber(const String number);
  bool isDigit(const char c);
  int charToInt(const char c);
  
  const short latchPin;
  const short clockPin;
  const short daiPin;
  
  public:
    Double7Segment(const short latchPin, const short clockPin, const short daiPin);
    
    /*
     * Display the given number in the 7 segment modules. 
     * The format can be either a single digit number or a double digit number
     * Examples:
     * 
     * "0"  -> first 7segment is off, second 7segment shows "0"
     * "15" -> first 7segment shows "1", second 7segment shows "5"
     * "5 " -> first 7segment shows "5", second 7 segment is off
     * 
     * You can add dots as you wish, for example:
     * "0."   -> first 7segment is off, second 7segment shows "0."
     * "5.6." -> first 7segment shows "5.", second 7segment shows "6."
     * "8.  " -> first 7segment shows "8.", second 7segment is off
     * 
	 * You can use hyphens, for example:
	 * "-5"  -> first 7segment is "-", second 7segment is "5"
	 * "--"  -> both of 7 segments are "-"
     * Attention: input's length must be single chrachter or even, for example, ".5." is not valid but " .5." is valid.
     */
    void display(const String number);

    /*
     * Display the given number, in int, in the 7 segment modules.
     * The expected format is a single digit numner or a double sigits numner.
     * This method support native numbers only, i.e there is has no option to turn 
     * on the dots segments, and there is no option to turn on only the first 7segment.
     * 
     * 0  -> first 7segment is off, second 7segment shows "0"
     * 15 -> first 7segment shows "1", second 7 segment show "5"
     */
    void display(const int number);
};

#endif