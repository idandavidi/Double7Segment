#include "Double7Segment.h"

Double7Segment::Double7Segment(const short latchPin, const short clockPin, const short daiPin): 
  latchPin(latchPin), clockPin(clockPin), daiPin(daiPin) {
  
}

void Double7Segment::display(const String number) {
  const String formattedNumber = convertToFormattedNumber(number);

  if (formattedNumber.length() != 4) {
    Serial.println(number + " is not a valid input to the Double7Segment library.");
    return;
  }

  byte digit1 = DIGIT_OFF;
  byte digit2 = DIGIT_OFF;

  if (formattedNumber[0] == '-') {
	digit1 = HYPHEN;
  } else if (formattedNumber[0] != ' ') {
    digit1 = numbers[charToInt(formattedNumber[0])];
  }

  if (formattedNumber[1] == '.') {
    digit1 = digit1 ^ DOT_SEGMENT_MASK;
  }
  
  
  
  if (formattedNumber[2] == '-') {
	digit2 = HYPHEN;
  } else if (formattedNumber[2] != ' ') {
    digit2 = numbers[charToInt(formattedNumber[2])];
  }

  if (formattedNumber[3] == '.') {
    digit2 = digit2 ^ DOT_SEGMENT_MASK;
  }

  // Clear the surface before writing
  shiftOut(this->daiPin, this->clockPin, MSBFIRST, DIGIT_OFF);
  shiftOut(this->daiPin, this->clockPin, MSBFIRST, DIGIT_OFF);
  digitalWrite(this->latchPin,HIGH);
  digitalWrite(this->latchPin,LOW);
  
  // Writing the actual digits
  shiftOut(this->daiPin, this->clockPin, MSBFIRST, digit1);
  shiftOut(this->daiPin, this->clockPin, MSBFIRST, digit2);
  digitalWrite(this->latchPin,HIGH);
  digitalWrite(this->latchPin,LOW);
}

void Double7Segment::display(const int number) {
  String strNumber = String(number);
  this->display(strNumber);
}

String Double7Segment::convertToFormattedNumber(const String number) {
  String formattedNumber = "";
  
  for (int i=0; i<number.length(); ++i) {
    char c = number[i];

    formattedNumber += c;
    
    if (isDigit(c)) {
      if ((i < number.length() -1) && isDigit(number[i+1])) {
        formattedNumber += " ";
      } else if (i == number.length() -1) {
        formattedNumber += " ";
      }
    }
  }

  if (formattedNumber.length() == 2) {
    formattedNumber = "  " + formattedNumber;
  }
  
  return formattedNumber;
}

bool Double7Segment::isDigit(const char c) {
  return (c >='0' && c<= '9') || (c == '-');
}

int Double7Segment::charToInt(const char c) {
  return c - '0';
}